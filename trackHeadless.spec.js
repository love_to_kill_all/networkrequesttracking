
describe('MyApp', () => {
    // We declare an empty array to gather XHR responses
    const xhrData = [];
    after(() => {
      // In record mode, save gathered XHR data to local JSON file
      if (Cypress.env('RECORD')) {
        cy.writeFile('records.json', xhrData);
      }
    });
    function track($xhr) {
      var temp = [];
      cy.window().wait(100).then((win) => {
          var x = win.performance.getEntriesByType('resource');
          x.forEach(($el, index, $list) => {
              temp = $el.name.split("/");
              $xhr.push({ fileName: temp[temp.length-1], url: ($el.name), Type: ($el.initiatorType) });
              cy.log(temp[temp.length-1] + ", a " + $el.initiatorType+" is requested");
          })
      })
      return $xhr;
    }
    it('Work', () => {			
      cy.server({
        // Here we handle all requests passing through Cypress' server
        onResponse: (response) => {
          if (Cypress.env('RECORD')) {
            // We push a new entry into the xhrData array
            
            xhrData.push({ "method": response.method, "url": response.url, "data": response.body});
            cy.writeFile(path, xhrData);
          }
        },
      });
      // This tells Cypress to hook into any GET request
      if (Cypress.env('RECORD')) {
        cy.route({
          method: 'GET',
          url: '*',
        });
      }
      // Load stubbed data from local JSON file
      if (!Cypress.env('RECORD')) {
        cy.fixture('fixture')
          .then((data) => {
            for (let i = 0, length = data.length; i < length; i++) {
              cy.route(data[i].method, data[i].url, data[i].data).as("x").wait("@x");
            }
          });
      }
      var headlessBody="<script src='/jquery.js'></script><img src='/src/img/a.jpeg'><h1>headless</h1>";
      cy.window().its('document').invoke('write', headlessBody);
            track(xhrData); // To get static resources
    });
  });